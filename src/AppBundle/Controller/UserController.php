<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Photo;
use AppBundle\Form\PhotoType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class UserController extends Controller
{
    /**
     * @Route("/user/{id}", requirements={"id": "\d+"})
     * @param $id integer
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(int $id)
    {
        $user = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->find($id);
        $photos = $this->getUser()->getPhotos();

        return $this->render('AppBundle:User:index.html.twig', [
            'user' => $user,
            'photos' => $photos]);
    }

    /**
     * @Route("/photos/new")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createPhotoAction(Request $request)
    {
        $photo = new Photo();
        $user = $this->getUser();

        $form = $this->createForm(PhotoType::class, $photo, [
            'method' => 'POST'
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $photo = $form->getData();
            $photo->setUser($this->getUser());

            $em = $this->getDoctrine()->getManager();
            $em->persist($photo);
            $em->flush();



            return $this->redirectToRoute('app_user_index', array(
                'id' => $user->getId()
            ));
        }
        return $this->render('AppBundle:User:new.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/photo/{id}", requirements={"id": "\d+"})
     * @param $id integer
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction(int $id)
    {
        $photo = $this->getDoctrine()
            ->getRepository('AppBundle:Photo')
            ->find($id);

        $user = $this->getUser();

        $em = $this->getDoctrine()->getManager();
        $em->remove($photo);
        $em->flush();
        return $this->redirectToRoute("app_user_index", [
            'photo' => $photo,
            'id' => $user->getId()]);
    }
}
