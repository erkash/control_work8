<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class BasicController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        $photos = $this->getDoctrine()
            ->getRepository('AppBundle:Photo')
            ->findAll();


        return $this->render('AppBundle:Basic:index.html.twig', array(
            'user' => $this->getUser(),
            'photos' => $photos
        ));
    }

}
